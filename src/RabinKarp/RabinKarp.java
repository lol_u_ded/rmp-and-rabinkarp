package RabinKarp;

import java.util.ArrayList;
import java.util.List;

public class RabinKarp {

	public static int HASH_BASE = 31;

	/**
	 * This method uses the RabinKarp algorithm to search a given pattern in a given input text.
	 *
	 * @param pattern - The pattern that is searched in the text.
	 * @param text    - The text in which the pattern is searched.
	 * @return Result class containing the number of occurrences of the pattern in the text, as well as
	 * the start indices of these occurrences in an array.
	 * @throws IllegalArgumentException if pattern or text is null.
	 */
	static public Result search (String pattern, String text) throws IllegalArgumentException {
		// Error cases
		if (pattern == null || text == null) {
			throw new IllegalArgumentException("Either pattern or text is null");
		}
		if (text.equals("**")) {
			throw new IllegalArgumentException("Given Text is empty.");
		}
		if (pattern.equals("")) {
			throw new IllegalArgumentException("There is no pattern to check.");
		}

		final int patternHash = getHashValue(pattern.toCharArray());
		String subString = text.substring(0, pattern.length());
		int subStringHash = getHashValue(subString.toCharArray());

		List<Integer> result = new ArrayList<>();

		for (int i = 1; i <= text.length() - pattern.length(); i++) {
			if (patternHash == subStringHash) {
				boolean match = true;
				for (int j = 0; j < pattern.length(); j++) {
					if (subString.charAt(j) != pattern.charAt(j)) {
						match = false;
						break;
					}
				}
				if (match) {
					result.add(i - 1);
				}
			}
			subString = text.substring(i, i + pattern.length());
			subStringHash = getRollingHashValue(subString.toCharArray(), text.charAt(i - 1), subStringHash);
		}
		return new Result(result.toArray(Integer[]::new), result.size());
	}

		
	/**
	 * This method calculates the (rolling) hash code for a given character sequence. For the calculation
	 * use the base b=31.
	 *
	 * @param sequence - The char sequence for which the (rolling) hash shall be computed.
	 * @param lastCharacter - The character that shall be removed when the hash for the next 
	 * 					character sequence is calculated. 
	 * @param previousHash - The previously calculated hash value that will be reused for the new hash values.
	 * @return calculated hash value for the given character sequence.
	 */
	static public int getRollingHashValue(char[] sequence, char lastCharacter, int previousHash) {
		//ErrorCase
		if (sequence == null) {
			return -1;
		}
		int length = sequence.length;
		if (previousHash == 0) {
			return getHashValue(sequence);
		} else {
			return (int) ((previousHash * HASH_BASE)
					- (lastCharacter * Math.pow(HASH_BASE, length))
					+ sequence[length - 1]);
		}
	}

	static private int getHashValue(char[] sequence) {
		int hashValue = 0;
		int length = sequence.length;
		for (int i = 0; i < sequence.length; i++) {
			char c = sequence[i];
			length--;
			hashValue += c * Math.pow(HASH_BASE, length);
		}
		return hashValue;
	}
}
