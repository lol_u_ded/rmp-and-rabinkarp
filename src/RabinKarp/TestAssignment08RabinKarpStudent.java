package RabinKarp;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Comparator;

import org.junit.jupiter.api.Test;

public class TestAssignment08RabinKarpStudent {
	private class IntegerComparator implements Comparator<Integer> { 
	    public int compare(Integer a, Integer b) { 
	        return a.compareTo(b); 
	    } 
	} 
		
	@Test
	public void testKMPShort() {
		Result result = RabinKarp.search ("xxx", "abcdexxxunbxxxxke");
		
		System.out.println();
		System.out.print("[Short] Size: " + result.count + "; Indices: ");
		for(Integer i : result.resultIndices)
			System.out.print(i+"; ");
		System.out.println();

		assertTrue(result.count > 0);
		assertEquals(3, result.count);

		int[] targets = {5, 11, 12};
		Integer[] indices = result.resultIndices;
		Arrays.sort(indices, new IntegerComparator());

		boolean resultCorrect = true;
		int targetToFind = 0;
		for (Integer index : indices) {
			if (targetToFind >= targets.length) resultCorrect = false;
			else if (index == targets[targetToFind]) targetToFind++;
		}
		if (targetToFind < targets.length) resultCorrect = false;

		assertTrue(resultCorrect);

	}
	
	@Test
	public void testKMPSpecial() {
		Result result = RabinKarp.search ("xxx", "xxxxxA");
		
		System.out.println();
		System.out.print("[Special] Size: " + result.count + "; Indices: ");
		for(Integer i : result.resultIndices)
			System.out.print(i+"; ");
		System.out.println();
		
		assertTrue(result.count > 0);
		assertEquals(3, result.count);
		
		int[] targets = {0,1,2};
		Integer[] indices = result.resultIndices;
		Arrays.sort(indices, new IntegerComparator());
		
		boolean resultCorrect = true;
		int targetToFind = 0;
		for(int i = 0; i<indices.length; i++) {
			if(targetToFind >= targets.length) resultCorrect = false;
			else if(indices[i].intValue() == targets[targetToFind]) targetToFind++;
		}
		if(targetToFind<targets.length) resultCorrect = false;
		
		assertTrue(resultCorrect);
	}
	
		
	@Test
	public void testRabinKarpHashOfPattern() {
		String strPattern = "ef";
		
		int hashPattern = RabinKarp.getRollingHashValue(strPattern.toCharArray(), '\0', 0);
		assertEquals(3233,hashPattern);
	}
	
	@Test
	public void testRabinKarpHashOfTextSequences() {
		String strText = "abcdef";
		int hash = 0;
		
		hash = RabinKarp.getRollingHashValue(strText.substring(0, 2).toCharArray(), '\0', 0);
		assertEquals(3105, hash);
		
		hash = RabinKarp.getRollingHashValue(strText.substring(1, 3).toCharArray(), strText.charAt(0), hash);
		assertEquals(3137,hash);
		
		hash = RabinKarp.getRollingHashValue(strText.substring(2, 4).toCharArray(), strText.charAt(1), hash);
		assertEquals(3169,hash);
		
		hash = RabinKarp.getRollingHashValue(strText.substring(3, 5).toCharArray(), strText.charAt(2), hash);
		assertEquals(3201,hash);
		
		hash = RabinKarp.getRollingHashValue(strText.substring(4, 6).toCharArray(), strText.charAt(3), hash);
		assertEquals(3233,hash);
	}
}
