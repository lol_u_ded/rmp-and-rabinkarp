package KMP;

public class Result {
	public Integer resultIndices[];		// array containing the start indices of pattern occurrences
	public int count;			// number of occurrences 
	
	public Result(Integer resultIndices[], int count) {
		this.resultIndices = resultIndices.clone();
		this.count = (count);
	}
}