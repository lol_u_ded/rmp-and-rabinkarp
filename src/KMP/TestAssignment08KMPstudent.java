package KMP;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Comparator;

import org.junit.jupiter.api.Test;

public class TestAssignment08KMPstudent {
	private class IntegerComparator implements Comparator<Integer> { 
	    public int compare(Integer a, Integer b) { 
	        return a.compareTo(b); 
	    } 
	} 
	
	@Test
	public void testKMPFailureTable() {
		String strPattern = "ababac";
		int[] failureTable = KMP.getFailureTable(strPattern);
		
		assertEquals(0,failureTable[0]);
		assertEquals(0,failureTable[1]);
		assertEquals(1,failureTable[2]);
		assertEquals(2,failureTable[3]);
		assertEquals(3,failureTable[4]);
		assertEquals(0,failureTable[5]);
	}
	
	@Test
	public void testKMPShort() {
		Result result = KMP.search ("xxx", "abcdexxxunbxxxxke");
		
		System.out.println();
		System.out.print("[Short] Size: " + result.count + "; Indices: ");
		for(Integer i : result.resultIndices)
			System.out.print(i+"; ");
		System.out.println();
		
		assertTrue(result.count > 0);
		assertEquals(3, result.count);

		int[] targets = {5,11,12};
		Integer[] indices = result.resultIndices;
		Arrays.sort(indices, new IntegerComparator());
		
		boolean resultCorrect = true;
		int targetToFind = 0;
		for(int i = 0; i<indices.length; i++) {
			if(targetToFind >= targets.length) resultCorrect = false;
			else if(indices[i].intValue() == targets[targetToFind]) targetToFind++;
		}
		if(targetToFind<targets.length) resultCorrect = false;
		
		assertTrue(resultCorrect);
		
	}
	
	@Test
	public void testKMPSpecial() {
		Result result = KMP.search ("xxx", "xxxxxA");
		
		System.out.println();
		System.out.print("[Special] Size: " + result.count + "; Indices: ");
		for(Integer i : result.resultIndices)
			System.out.print(i+"; ");
		System.out.println();
		
		assertTrue(result.count > 0);
		assertEquals(3, result.count);
		
		int[] targets = {0,1,2};
		Integer[] indices = result.resultIndices;
		Arrays.sort(indices, new IntegerComparator());
		
		boolean resultCorrect = true;
		int targetToFind = 0;
		for(int i = 0; i<indices.length; i++) {
			if(targetToFind >= targets.length) resultCorrect = false;
			else if(indices[i].intValue() == targets[targetToFind]) targetToFind++;
		}
		if(targetToFind<targets.length) resultCorrect = false;
		
		assertTrue(resultCorrect);
	}
	
	// Todo: Think about and implement other special and longer testcases to verify your implementation.
}
