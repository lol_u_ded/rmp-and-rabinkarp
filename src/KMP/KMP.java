package KMP;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KMP {

    static int[] failureTable;

    /**
     * This method uses the KMP algorithm to search a given pattern in a given input text.
     *
     * @param pattern - The pattern that is searched in the text.
     * @param text    - The text in which the pattern is searched.
     * @return Result class containing the number of occurrences of the pattern in the text, as well as
     * the start indices of these occurrences in an array.
     * @throws IllegalArgumentException if pattern or text is null.
     */
    static public Result search(String pattern, String text) throws IllegalArgumentException {
        // Error cases
        if (pattern == null || text == null) {
            throw new IllegalArgumentException("Either pattern or text is null");
        }
        if (text.equals("**")) {
            throw new IllegalArgumentException("Given Text is empty.");
        }
        if (pattern.equals("")) {
            throw new IllegalArgumentException("There is no pattern to check.");
        }

        failureTable = getFailureTable(pattern);

        List<Integer> resultList = new ArrayList<>();
        final int next = 1;
        int i = 0;
        int deleted = 0;


        if (i < text.length()) {
            do {

                String substring = text.substring(i);
                int index = findKMP(pattern, substring);

                if (index >= 0) {
                    resultList.add((index + deleted));
                    i = index + next + deleted;
                    deleted = deleted + index + next;
                } else {
                    return new Result(resultList.toArray(Integer[]::new), resultList.size());
                }
            } while (i < text.length());
        }
        return new Result(resultList.toArray(Integer[]::new), resultList.size());
    }

    /**
     * This method calculates and returns the failure table for a given pattern.
     *
     * @param pattern - The pattern for which the failure table shall be calculated.
     * @return int[] containing the failure table for the given pattern.
     * @throws IllegalArgumentException if pattern is null.
     */
    public static int[] getFailureTable(String pattern) throws IllegalArgumentException {
        int pLength = pattern.length();
        int[] failPattern = new int[pLength];
        int i = 1;
        int j = 0;
        if (i < pLength) {
            do {
                if (pattern.charAt(i) == pattern.charAt(j)) {
                    failPattern[i] = j + 1;
                    i++;
                    j++;
                } else if (j > 0) {
                    j = failPattern[j - 1];
                } else {
                    i++;
                }
            } while (i < pLength);
        }
        return failPattern;
    }

    public static int findKMP(String pattern, String text) {
        int tLength = text.length();
        int pLength = pattern.length();
        if (pLength == 0) {
            return 0;
        }
        int i = 0;
        int j = 0;
        if (i < tLength) {
            do {
                if (text.charAt(i) == pattern.charAt(j)) {
                    if (j == pLength - 1) {
                        return i - pLength + 1;
                    }
                    i++;
                    j++;
                } else if (j > 0) {
                    j = failureTable[j - 1];
                } else {
                    i++;
                }
            } while (i < tLength);
        }
        return -1;
    }

}
